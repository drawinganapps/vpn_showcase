import 'package:flutter/material.dart';

class ColorHelper {
  static Color white = const Color.fromRGBO(255,255,255, 1);
  static Color whiteDarker = const Color.fromRGBO(238, 240, 244, 1);
  static Color grey = const Color.fromRGBO(208, 210, 212, 1);
  static Color primary = const Color.fromRGBO(246, 203, 98, 1);
  static Color secondary = const Color.fromRGBO(248, 133, 110, 1.0);
  static Color tertiary = const Color.fromRGBO(52, 44, 35, 1);
  static Color quaternary = const Color.fromRGBO(80, 59, 34, 1);
  static Color dark = const Color.fromRGBO(54, 49, 76, 1);
  static Color lightDark = const Color.fromRGBO(70, 63, 93, 1);
  static Color green = const Color.fromRGBO(63, 214, 145, 1);
}
