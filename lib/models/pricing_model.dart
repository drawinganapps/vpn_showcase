class PricingModel {
  int id;
  int durationInMonth;
  double price;

  PricingModel(
      {required this.id, required this.durationInMonth, required this.price});
}
