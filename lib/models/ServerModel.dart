class ServerModel {
  int id;
  String name;
  String icon;
  bool isPremium;

  ServerModel(
      {required this.id,
      required this.name,
      required this.icon,
      required this.isPremium});
}
