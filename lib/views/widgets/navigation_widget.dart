import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:vpn_showcase/helper/color_helper.dart';
import 'package:vpn_showcase/routes/AppRoutes.dart';

class NavigationWidget extends StatelessWidget {
  final int selectedMenu;

  const NavigationWidget({super.key, required this.selectedMenu});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;

    Widget selectedItem(IconData icon, String name) {
      return Container(
        height: heightSize * 0.1,
        width: widthSize * 0.3,
        decoration: BoxDecoration(
            color: ColorHelper.white, borderRadius: BorderRadius.circular(30)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Icon(icon, size: 25, color: ColorHelper.primary),
            Text(name,
                style: TextStyle(
                    color: ColorHelper.dark,
                    fontWeight: FontWeight.w600,
                    fontSize: 16))
          ],
        ),
      );
    }

    Widget itemMenu(IconData icon) {
      return Container(
        decoration: const BoxDecoration(shape: BoxShape.circle),
        padding: EdgeInsets.all(heightSize * 0.015),
        child: Icon(icon, size: 25, color: ColorHelper.white),
      );
    }

    return Container(
      color: ColorHelper.white,
      height: heightSize * 0.1,
      padding: EdgeInsets.only(
          left: widthSize * 0.03,
          right: widthSize * 0.03,
          bottom: heightSize * 0.02),
      child: Container(
        decoration: BoxDecoration(
            color: ColorHelper.dark, borderRadius: BorderRadius.circular(35)),
        padding: EdgeInsets.all(widthSize * 0.02),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            GestureDetector(
              child: selectedMenu == 0 ? selectedItem(Icons.home_rounded, 'Home') : itemMenu(Icons.home_rounded),
              onTap: () => context.go(AppRoutes.HOME),
            ),
            GestureDetector(
              child: selectedMenu == 1 ? selectedItem(Icons.public, 'Servers') : itemMenu(Icons.public),
              onTap: () => context.go(AppRoutes.SERVER_LIST),
            ),
            itemMenu(Icons.bolt),
            itemMenu(Icons.settings_rounded)
          ],
        ),
      ),
    );
  }
}
