import 'package:flutter/material.dart';
import 'package:vpn_showcase/helper/color_helper.dart';

class SpeedCardWidget extends StatelessWidget {
  final int state;

  const SpeedCardWidget({super.key, required this.state});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;
    return Container(
      height: heightSize * 0.12,
      padding: const EdgeInsets.only(left: 2, right: 2, bottom: 2, top: 8),
      decoration: BoxDecoration(
          color: state == 0 ? ColorHelper.secondary : ColorHelper.primary,
          borderRadius: BorderRadius.circular(30)),
      child: Container(
        padding: EdgeInsets.only(left: widthSize * 0.05, right: widthSize * 0.05),
        decoration: BoxDecoration(
            color: ColorHelper.white, borderRadius: BorderRadius.circular(30)),
        child: Row(
          children: [
            Container(
              margin: EdgeInsets.only(right: widthSize * 0.02),
              padding: const EdgeInsets.all(2),
              decoration: BoxDecoration(
                  color:
                      state == 0 ? ColorHelper.secondary : ColorHelper.primary,
                  shape: BoxShape.circle),
              child: const Icon(Icons.arrow_upward, size: 18),
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: EdgeInsets.only(bottom: heightSize * 0.005),
                  child: Text(state == 0 ? 'DOWNLOADS' : 'UPLOAD', style: TextStyle(
                      color: ColorHelper.grey,
                      fontSize: 12
                  )),
                ),
                Text('50.2 Mb/S', style: TextStyle(
                  color: ColorHelper.dark,
                  fontSize: 16,
                  fontWeight: FontWeight.w600
                )),
              ],
            )
          ],
        ),
      ),
    );
  }
}
