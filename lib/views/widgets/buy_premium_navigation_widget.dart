import 'package:flutter/cupertino.dart';
import 'package:vpn_showcase/helper/color_helper.dart';

class BuyPremiumNavigationWidget extends StatelessWidget {
  const BuyPremiumNavigationWidget({super.key});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;

    return Container(
      height: heightSize * 0.1,
      padding: EdgeInsets.only(left: widthSize * 0.05, right: widthSize * 0.05, bottom: heightSize * 0.015, top: heightSize * 0.015),
      decoration: BoxDecoration(
        color: ColorHelper.dark
      ),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
          gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.topRight,
              colors: [
                ColorHelper.secondary,
                ColorHelper.primary,
              ]),
        ),
        child: Center(
          child: Text('Buy Premium', style: TextStyle(
            color: ColorHelper.white,
            fontWeight: FontWeight.w600
          )),
        ),
      ),
    );
  }
}
