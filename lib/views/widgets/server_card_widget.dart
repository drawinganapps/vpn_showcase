import 'package:flutter/material.dart';
import 'package:vpn_showcase/helper/color_helper.dart';
import 'package:vpn_showcase/models/ServerModel.dart';

class ServerCardWidget extends StatelessWidget {
  final ServerModel server;

  const ServerCardWidget({super.key, required this.server});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;

    Widget freeServer(bool isSelected) {
      return Container(
        padding: const EdgeInsets.all(4),
        height: heightSize * 0.025,
        width: heightSize * 0.025,
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            border: Border.all(color: ColorHelper.whiteDarker)),
        child: Container(
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              color: isSelected ? ColorHelper.primary : ColorHelper.white),
        ),
      );
    }

    Widget premiumServer() {
      return Icon(Icons.lock, color: ColorHelper.primary);
    }

    return Container(
      height: 60,
      padding: const EdgeInsets.all(1),
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.topRight,
              colors: [
                ColorHelper.secondary,
                ColorHelper.primary,
              ]),
          borderRadius: BorderRadius.circular(35)),
      child: Container(
        padding: const EdgeInsets.only(left: 5, top: 5, bottom: 5, right: 10),
        decoration: BoxDecoration(
            color: ColorHelper.white, borderRadius: BorderRadius.circular(35)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              children: [
                Container(
                  padding: const EdgeInsets.all(2),
                  margin: EdgeInsets.only(right: widthSize * 0.03),
                  decoration: BoxDecoration(
                      color: ColorHelper.whiteDarker, shape: BoxShape.circle),
                  child: Image.asset('assets/icons/${server.icon}',
                      fit: BoxFit.cover),
                ),
                Text(server.name,
                    style: TextStyle(
                        fontSize: 18,
                        color: ColorHelper.dark,
                        fontWeight: FontWeight.w600))
              ],
            ),
            Row(
              children: [
                Icon(Icons.signal_cellular_alt, color: ColorHelper.green),
                Container(
                  margin: EdgeInsets.only(left: widthSize * 0.01),
                  child: server.isPremium ? premiumServer() : freeServer(server.id == 1),
                )
              ],
            )
          ],
        ),
      ),
    );
  }
}
