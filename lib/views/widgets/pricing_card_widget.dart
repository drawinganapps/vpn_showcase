import 'package:flutter/cupertino.dart';
import 'package:vpn_showcase/helper/color_helper.dart';
import 'package:vpn_showcase/models/pricing_model.dart';

class PricingCardWidget extends StatelessWidget {
  final PricingModel pricing;
  final bool isSelected;

  const PricingCardWidget(
      {super.key, required this.pricing, required this.isSelected});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;

    BoxDecoration selectedParent() {
      return BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        gradient: LinearGradient(
            begin: Alignment.topLeft,
            end: Alignment.topRight,
            colors: [
              ColorHelper.secondary,
              ColorHelper.primary,
            ]),
      );
    }

    BoxDecoration unSelectedParent() {
      return BoxDecoration(
        borderRadius: BorderRadius.circular(20),
      );
    }

    return Container(
      height: heightSize * 0.1,
      padding: const EdgeInsets.all(1),
      decoration: isSelected ? selectedParent() : unSelectedParent(),
      child: Container(
        padding:
            EdgeInsets.only(left: widthSize * 0.03, right: widthSize * 0.03),
        decoration: BoxDecoration(
            color: ColorHelper.lightDark,
            borderRadius: BorderRadius.circular(20)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  padding: const EdgeInsets.all(2),
                  height: heightSize * 0.02,
                  width: heightSize * 0.02,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(color: ColorHelper.dark)),
                  child: Container(
                    decoration:
                        isSelected ? selectedParent() : unSelectedParent(),
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(
                      left: widthSize * 0.05, right: widthSize * 0.05),
                  child: Text(pricing.durationInMonth.toString(),
                      style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 25,
                          color: ColorHelper.white)),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Month',
                        style: TextStyle(
                            color: ColorHelper.white,
                            fontSize: 14,
                            fontWeight: FontWeight.w600)),
                    Text(
                        'Total Price \$ ${(pricing.price * pricing.durationInMonth).toStringAsFixed(2)}',
                        style: TextStyle(
                            color: ColorHelper.grey,
                            fontSize: 8,
                            fontWeight: FontWeight.w600))
                  ],
                )
              ],
            ),
            Text('\$ ${pricing.price.toStringAsFixed(2)}',
                style: TextStyle(
                    color: ColorHelper.white,
                    fontSize: 16,
                    fontWeight: FontWeight.w600))
          ],
        ),
      ),
    );
  }
}
