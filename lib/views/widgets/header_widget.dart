import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:vpn_showcase/helper/color_helper.dart';
import 'package:vpn_showcase/routes/AppRoutes.dart';

class HeaderWidget extends StatelessWidget {
  const HeaderWidget({super.key});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          height: widthSize * 0.13,
          width: widthSize * 0.13,
          decoration: BoxDecoration(
              color: ColorHelper.whiteDarker,
              shape: BoxShape.circle
          ),
          child: Image.asset('assets/icons/avatar.png'),
        ),
        Container(
          padding: EdgeInsets.only(
              left: widthSize * 0.05,
              right: widthSize * 0.05,
              top: heightSize * 0.015,
              bottom: heightSize * 0.015),
          decoration: BoxDecoration(
              color: ColorHelper.primary,
              borderRadius: BorderRadius.circular(25)),
          child: GestureDetector(
            onTap: () => context.go(AppRoutes.PRICING),
            child: Row(
              children: [
                Container(
                  margin: EdgeInsets.only(right: widthSize * 0.01),
                  child: Icon(Icons.workspace_premium, color: ColorHelper.dark),
                ),
                Text('GET PREMIUM',
                    style: TextStyle(
                        color: ColorHelper.dark,
                        fontSize: 14,
                        fontWeight: FontWeight.w600))
              ],
            ),
          ),
        ),
        Container(
          padding: EdgeInsets.all(widthSize * 0.03),
          decoration: BoxDecoration(
            color: ColorHelper.whiteDarker,
            shape: BoxShape.circle
          ),
          child: Icon(Icons.notifications_outlined, color: ColorHelper.dark),
        )
      ],
    );
  }
}
