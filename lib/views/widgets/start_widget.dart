import 'package:flutter/material.dart';
import 'package:vpn_showcase/helper/color_helper.dart';

class StartWidget extends StatelessWidget {
  const StartWidget({super.key});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;
    return Container(
      padding: const EdgeInsets.all(8),
      height: heightSize * 0.2,
      width: widthSize * 0.5,
      decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                ColorHelper.lightDark,
                ColorHelper.lightDark,
                ColorHelper.lightDark,
                ColorHelper.secondary,
              ]),
          shape: BoxShape.circle),
      child: Container(
        padding: const EdgeInsets.all(8),
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.topRight,
                colors: [
                  ColorHelper.secondary,
                  ColorHelper.primary,
                ])),
        child: Container(
          padding: const EdgeInsets.all(8),
          decoration:
              BoxDecoration(color: ColorHelper.white, shape: BoxShape.circle),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Icon(Icons.power_settings_new,
                  color: ColorHelper.secondary, size: 30),
              Container(
                margin: EdgeInsets.only(
                    top: heightSize * 0.01, bottom: heightSize * 0.01),
                child: const Text('STOP',
                    style: TextStyle(fontWeight: FontWeight.w600)),
              ),
              Text('00:45:35',
                  style: TextStyle(
                      fontWeight: FontWeight.w500,
                      color: ColorHelper.grey,
                      fontSize: 18)),
            ],
          ),
        ),
      ),
    );
  }
}
