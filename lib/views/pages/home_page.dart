import 'package:flutter/cupertino.dart';
import 'package:vpn_showcase/helper/color_helper.dart';
import 'package:vpn_showcase/resources/dummy_data.dart';
import 'package:vpn_showcase/views/widgets/header_widget.dart';
import 'package:vpn_showcase/views/widgets/server_card_widget.dart';
import 'package:vpn_showcase/views/widgets/speed_card_widget.dart';
import 'package:vpn_showcase/views/widgets/start_widget.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;
    return Stack(
      children: [
        SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.only(top: heightSize * 0.03),
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomCenter,
                    colors: [
                      ColorHelper.dark,
                      ColorHelper.dark,
                      ColorHelper.secondary,
                      ColorHelper.secondary,
                    ]),
                image: const DecorationImage(
                    image: AssetImage('assets/icons/bg.png'),
                    opacity: 0.1,
                    fit: BoxFit.cover),
                color: ColorHelper.dark),
            child: Column(
              children: [
                Container(
                  padding: EdgeInsets.only(
                      left: widthSize * 0.05, right: widthSize * 0.05),
                  height: heightSize * 0.4,
                  width: widthSize,
                  child: Column(
                    children: [
                      const HeaderWidget(),
                      Container(
                        height: heightSize * 0.12,
                        margin: EdgeInsets.only(top: heightSize * 0.08),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text('IP-254.893.323',
                                style: TextStyle(color: ColorHelper.white)),
                            Text('United States',
                                style: TextStyle(
                                    color: ColorHelper.white,
                                    fontWeight: FontWeight.w600,
                                    fontSize: 32)),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text('Status: ',
                                    style: TextStyle(color: ColorHelper.white)),
                                Text('Connected',
                                    style: TextStyle(
                                        color: ColorHelper.secondary)),
                              ],
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(
                      left: widthSize * 0.05,
                      right: widthSize * 0.05,
                      top: heightSize * 0.15,
                      bottom: heightSize * 0.05),
                  width: widthSize,
                  height: heightSize * 0.48,
                  decoration: BoxDecoration(
                      color: ColorHelper.white,
                      borderRadius: const BorderRadius.only(
                        topRight: Radius.circular(50),
                        topLeft: Radius.circular(50),
                      )),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          const Expanded(child: SpeedCardWidget(state: 0)),
                          Container(
                            margin: EdgeInsets.only(
                                left: widthSize * 0.025,
                                right: widthSize * 0.025),
                          ),
                          const Expanded(child: SpeedCardWidget(state: 1)),
                        ],
                      ),
                      ServerCardWidget(server: DummyData.freeServer[0])
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
        Positioned(
            right: widthSize * 0.25,
            top: heightSize * 0.33,
            child: const StartWidget())
      ],
    );
  }
}
