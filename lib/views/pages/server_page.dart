import 'package:flutter/cupertino.dart';
import 'package:vpn_showcase/resources/dummy_data.dart';
import 'package:vpn_showcase/views/widgets/header_widget.dart';
import 'package:vpn_showcase/views/widgets/server_card_widget.dart';

class ServerPage extends StatelessWidget {
  const ServerPage({super.key});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;
    return Container(
      padding: EdgeInsets.only(top: heightSize * 0.03),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.only(
                left: widthSize * 0.05, right: widthSize * 0.05),
            child: const HeaderWidget(),
          ),
          Container(
            padding: EdgeInsets.only(
                left: widthSize * 0.05, right: widthSize * 0.05),
            margin: EdgeInsets.only(
                top: heightSize * 0.03, bottom: heightSize * 0.03),
            child: const Text('Free Server',
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600)),
          ),
          SizedBox(
              height: heightSize * 0.34,
              child: ListView(
                physics: const BouncingScrollPhysics(),
                children: List.generate(DummyData.freeServer.length, (index) {
                  return Container(
                    padding: EdgeInsets.only(
                        left: widthSize * 0.05, right: widthSize * 0.05),
                    child: Container(
                      margin: EdgeInsets.only(bottom: heightSize * 0.01),
                      child:
                          ServerCardWidget(server: DummyData.freeServer[index]),
                    ),
                  );
                }),
              )),
          Container(
            padding: EdgeInsets.only(
                left: widthSize * 0.05, right: widthSize * 0.05),
            margin: EdgeInsets.only(
                top: heightSize * 0.03, bottom: heightSize * 0.03),
            child: const Text('Premium Server',
                style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600)),
          ),
          Flexible(
              child: ListView(
            physics: const BouncingScrollPhysics(),
            children: List.generate(DummyData.premiumServer.length, (index) {
              return Container(
                padding: EdgeInsets.only(
                    left: widthSize * 0.05, right: widthSize * 0.05),
                child: Container(
                  margin: EdgeInsets.only(bottom: heightSize * 0.01),
                  child:
                      ServerCardWidget(server: DummyData.premiumServer[index]),
                ),
              );
            }),
          )),
        ],
      ),
    );
  }
}
