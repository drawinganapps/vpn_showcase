import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:vpn_showcase/helper/color_helper.dart';
import 'package:vpn_showcase/resources/dummy_data.dart';
import 'package:vpn_showcase/routes/AppRoutes.dart';
import 'package:vpn_showcase/views/widgets/pricing_card_widget.dart';

class PricingPage extends StatelessWidget {
  const PricingPage({super.key});

  @override
  Widget build(BuildContext context) {
    double widthSize = MediaQuery.of(context).size.width;
    double heightSize = MediaQuery.of(context).size.height;
    return Container(
        padding: EdgeInsets.only(top: heightSize * 0.03),
        decoration: BoxDecoration(
          color: ColorHelper.dark,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: widthSize,
              height: heightSize * 0.15,
              padding: EdgeInsets.only(
                  left: widthSize * 0.05, right: widthSize * 0.05),
              decoration: const BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('assets/icons/premium.png'),
                      fit: BoxFit.fitHeight)),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  GestureDetector(
                    onTap: ()=> context.go(AppRoutes.HOME),
                    child: Container(
                      padding: const EdgeInsets.all(10),
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: ColorHelper.whiteDarker.withOpacity(0.2)),
                      child: const Icon(Icons.cancel, size: 20),
                    ),
                  )
                ],
              ),
            ),
            Container(
              width: widthSize,
              padding: EdgeInsets.only(
                  left: widthSize * 0.05, right: widthSize * 0.05),
              margin: EdgeInsets.only(
                  top: heightSize * 0.03, bottom: heightSize * 0.03),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text('GET PREMIUM',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: ColorHelper.white,
                          fontWeight: FontWeight.w600,
                          fontSize: 24)),
                  Container(
                    margin: EdgeInsets.only(top: heightSize * 0.01),
                    child: Text('Remove Adds & Unlock All Location',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: ColorHelper.white,
                            fontWeight: FontWeight.w500,
                            fontSize: 11)),
                  )
                ],
              ),
            ),
            SizedBox(
              height: heightSize * 0.38,
              child: ListView(
                children:
                List.generate(DummyData.pricingAvailable.length, (index) {
                  return Stack(
                    children: [
                      Container(
                        padding: EdgeInsets.only(
                            left: widthSize * 0.05, right: widthSize * 0.05),
                        margin: EdgeInsets.only(bottom: heightSize * 0.02),
                        child: PricingCardWidget(
                            pricing: DummyData.pricingAvailable[index],
                            isSelected:
                            DummyData.pricingAvailable[index].id == 2),
                      ),
                      DummyData.pricingAvailable[index].id == 2
                          ? Positioned(
                        right: widthSize * 0.42,
                        top: heightSize * 0.09,
                        child: Container(
                          padding: const EdgeInsets.all(4),
                          decoration: BoxDecoration(
                              color: ColorHelper.primary,
                              borderRadius: BorderRadius.circular(20)),
                          child: Text('MOST POPULAR',
                              style: TextStyle(
                                  color: ColorHelper.dark,
                                  fontWeight: FontWeight.w600,
                                  fontSize: 6)),
                        ),
                      )
                          : Container()
                    ],
                  );
                }),
              ),
            ),
            Container(
              padding: EdgeInsets.only(
                  left: widthSize * 0.05, right: widthSize * 0.05),
              child: Column(
                children: List.generate(DummyData.premiumFeatures.length, (index) {
                  String featureText = DummyData.premiumFeatures[index];
                  return Container(
                    margin: EdgeInsets.only(bottom: heightSize * 0.015),
                    child: Row(
                      children: [
                        Container(
                          padding: const EdgeInsets.all(3),
                          margin: EdgeInsets.only(right: widthSize * 0.03),
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            gradient: LinearGradient(
                                begin: Alignment.topLeft,
                                end: Alignment.topRight,
                                colors: [
                                  ColorHelper.secondary,
                                  ColorHelper.primary,
                                ]),
                          ),
                          child: Icon(Icons.done, size: 12, color: ColorHelper.dark),
                        ),
                        Text(featureText, style: TextStyle(
                          color: ColorHelper.white,
                          fontSize: 14
                        ))
                      ],
                    ),
                  );
                }),
              ),
            )
          ],
        ));
  }
}
