import 'package:flutter/material.dart';
import 'package:vpn_showcase/views/pages/server_page.dart';
import 'package:vpn_showcase/views/widgets/navigation_widget.dart';

class ServerScreen extends StatelessWidget {
  const ServerScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: ServerPage(),
      bottomNavigationBar: NavigationWidget(selectedMenu: 1),
    );
  }
}
