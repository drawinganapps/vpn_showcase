import 'package:flutter/material.dart';
import 'package:vpn_showcase/views/pages/home_page.dart';
import 'package:vpn_showcase/views/widgets/navigation_widget.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: HomePage(),
      bottomNavigationBar: NavigationWidget(selectedMenu: 0),
    );
  }
}
