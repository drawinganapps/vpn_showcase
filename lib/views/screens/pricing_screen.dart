import 'package:flutter/material.dart';
import 'package:vpn_showcase/views/pages/pricing_page.dart';
import 'package:vpn_showcase/views/widgets/buy_premium_navigation_widget.dart';

class PricingScreen extends StatelessWidget {
  const PricingScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      body: PricingPage(),
      bottomNavigationBar: BuyPremiumNavigationWidget(),
    );
  }
}
