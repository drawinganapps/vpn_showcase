class AppRoutes {
  static const String HOME = '/home';
  static const String SERVER_LIST = '/server_list';
  static const String PRICING = '/pricing';
}
