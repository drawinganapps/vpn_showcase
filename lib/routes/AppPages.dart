import 'package:go_router/go_router.dart';
import 'package:vpn_showcase/routes/AppRoutes.dart';
import 'package:vpn_showcase/views/screens/home_screen.dart';
import 'package:vpn_showcase/views/screens/pricing_screen.dart';
import 'package:vpn_showcase/views/screens/server_screen.dart';

class AppPage {
// GoRouter configuration
  static var routes = GoRouter(
    initialLocation: AppRoutes.HOME,
    routes: [
      GoRoute(
        path: AppRoutes.HOME,
        builder: (context, state) => const HomeScreen(),
      ),
      GoRoute(
        path: AppRoutes.SERVER_LIST,
        builder: (context, state) => const ServerScreen(),
      ),
      GoRoute(
        path: AppRoutes.PRICING,
        builder: (context, state) => const PricingScreen(),
      ),
    ],
  );
}
