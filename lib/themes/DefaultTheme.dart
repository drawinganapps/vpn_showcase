import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:vpn_showcase/helper/color_helper.dart';

ThemeData defaultTheme = ThemeData(
    brightness: Brightness.dark,
    backgroundColor: ColorHelper.white,
    scaffoldBackgroundColor: ColorHelper.white,
    highlightColor: ColorHelper.white,
    splashColor: ColorHelper.white,
    primarySwatch: Colors.amber,
    textTheme: GoogleFonts.arimoTextTheme().copyWith(
    ),
);
