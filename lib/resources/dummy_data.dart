import 'package:vpn_showcase/models/ServerModel.dart';
import 'package:vpn_showcase/models/pricing_model.dart';

class DummyData {
  static List<ServerModel> freeServer = [
    ServerModel(id: 1, name: 'United States', icon: 'usa.png', isPremium: false),
    ServerModel(id: 2, name: 'Canada', icon: 'canada.png', isPremium: false),
    ServerModel(id: 3, name: 'Brazil', icon: 'brazil.png', isPremium: false),
    ServerModel(id: 4, name: 'Australia', icon: 'australia.png', isPremium: false)
  ];

  static List<ServerModel> premiumServer = [
    ServerModel(id: 5, name: 'Singapore', icon: 'singapore.png', isPremium: true),
    ServerModel(id: 6, name: 'Japan', icon: 'japan.png', isPremium: true),
    ServerModel(id: 7, name: 'China', icon: 'china.png', isPremium: true),
    ServerModel(id: 8, name: 'Germany', icon: 'germany.png', isPremium: true),
  ];

  static List<PricingModel> pricingAvailable = [
    PricingModel(id: 1, durationInMonth: 1, price: 50.00),
    PricingModel(id: 2, durationInMonth: 3, price: 45.00),
    PricingModel(id: 3, durationInMonth: 6, price: 40.00),
  ];

  static List<String> premiumFeatures = [
    'Remove all ads',
    'Unlock all locations',
    '24/7 primarily support',
    'Secure streaming & browsing',
  ];
}
